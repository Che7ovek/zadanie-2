//title: Товар с ТП в случайный каталог
\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('catalog');

$IBlocks = [];
$offersCount = \Bitrix\Main\Security\Random::getInt(1, 5);
$offersIDs = [];

//Выбираем из всех инфоблоков, все с торговыми предложениями
$dbIBlocks = CIBlock::GetList(
	["SORT"=>"ASC"],
	['ACTIVE' => 'Y']
);
while($arIBlock = $dbIBlocks->fetch())
{
	$arInfo = CCatalogSKU::GetInfoByProductIBlock($arIBlock['ID']);
	if($arInfo)
	{
		$arIBlock['OFFERS_IBLOCK'] = $arInfo['IBLOCK_ID'];
		$arIBlock['SKU_PROPERTY_ID'] = $arInfo['SKU_PROPERTY_ID'];
		$IBlocks[] = $arIBlock;
	}
}
if(!empty($IBlocks))
{
	//Выбираем случайного счастливчика
	$luckyIndex = \Bitrix\Main\Security\Random::getInt(0, count($IBlocks) - 1);
	$arLucky = $IBlocks[$luckyIndex];

	//Заполняем свойства дефолтными значениями
	$arFields = array(
		'MODIFIED_BY' => $GLOBALS['USER']->GetID(),
		'NAME' => 'Lucky',
		'CODE' => 'lucky'.\Bitrix\Main\Security\Random::getString(6),
		'IBLOCK_ID' => $arLucky['ID'],
	);
	$luckyFields = CIBlock::GetFields($arLucky['ID']);
	foreach($luckyFields as $code => $field)
	{
		if($field['IS_REQUIRED'] === 'Y' && empty($arFields[$code]))
		{
			// Обязательная привязка к разделам, значит нельзя к корню привязывать. Берём первый попавшийся
			if($code === 'IBLOCK_SECTION')
			{
			   $dbSection = CIBlockSection::GetList(
					array(),
					array("IBLOCK_ID" => $arLucky['ID'], "ACTIVE" => "Y"),
					false,
					array('ID'),
					array('nTopCount' => 1)
				);
			   if($arSection = $dbSection->fetch())
					$arFields[$code] = array($arSection['ID']);
			}
			else
				$arFields[$code] = $field['DEFAULT_VALUE'];
		}
	}

	$el = new CIBlockElement;

	if($PRODUCT_ID = $el->Add($arFields))
	{
		while($offersCount--)
		{
			//Заполняем свойства дефолтными значениями
			$arFields = array(
				'MODIFIED_BY' => $GLOBALS['USER']->GetID(),
				'NAME' => 'Lucky',
				'CODE' => 'lucky'.\Bitrix\Main\Security\Random::getString(6),
				'IBLOCK_ID' => $arLucky['OFFERS_IBLOCK'],
				'PROPERTY_VALUES' => array(
					$arLucky['SKU_PROPERTY_ID'] => $PRODUCT_ID, // Привязка к новому товару
				),
			);
			$luckyFields = CIBlock::GetFields($arLucky['OFFERS_IBLOCK']);
			foreach($luckyFields as $code => $field)
			{
				if($field['IS_REQUIRED'] === 'Y' && empty($arFields[$code]))
				{
					// Обязательная привязка к разделам, значит нельзя к корню привязывать. Берём первый попавшийся
					if($code === 'IBLOCK_SECTION')
					{
					   $dbSection = CIBlockSection::GetList(
							array(),
							array("IBLOCK_ID" => $arLucky['OFFERS_IBLOCK'], "ACTIVE" => "Y"),
							false,
							array('ID'),
							array('nTopCount' => 1)
						);
					   if($arSection = $dbSection->fetch())
							$arFields[$code] = array($arSection['ID']);
					}
					else
						$arFields[$code] = $field['DEFAULT_VALUE'];
				}
			}

			$OFFER_ID = $el->Add($arFields)
			// Если не создалось ТП, попробуем ещё раз (Если символьный код должен быть уникальным, то могло и совпасть случайно)
			if(!$OFFER_ID)
				$offersCount++;
		}
	}
	else
		echo "Шошибке: ".$el->LAST_ERROR;
}
else
	echo "Ни одного каталога с торговыми предложениями, сорьки";

